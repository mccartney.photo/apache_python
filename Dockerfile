FROM fedora:28

RUN dnf -y update && dnf clean all
RUN dnf -y install unixODBC && dnf clean all
RUN dnf -y install httpd && dnf clean all
RUN dnf -y install python3-mod_wsgi && dnf clean all
RUN dnf -y install nodejs && dnf clean all
RUN dnf -y install gcc && dnf clean all
RUN dnf -y install gcc-c++ && dnf clean all
RUN dnf -y install redhat-rpm-config && dnf clean all
RUN dnf -y install python3-devel && dnf clean all
RUN dnf -y install unixODBC-devel && dnf clean all
RUN dnf -y install mysql-devel && dnf clean all
RUN dnf -y groupinstall "Development Tools" && dnf clean all

COPY oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm /root/
CMD dnf install /root/oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm
CMD sh -c "echo /usr/lib/oracle/12.2/client64/lib > /etc/ld.so.conf.d/oracle-instantclient.conf"
CMD ldconfig